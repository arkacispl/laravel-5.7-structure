@extends('admin.layouts.app')

@section('content')
<div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
  
          <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
              <!-- Nested Row within Card Body -->
              <div class="row">
                <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                <div class="col-lg-6">
                  <div class="p-5">
                    <div class="text-center">
                      <h1 class="h4 text-gray-900 mb-4">Eshoppe Admin</h1>
                    </div>
                    <form class="user" action="{{ route('admin_do_login') }}" method="POST">
                      {!! csrf_field() !!}
                      <div class="form-group">
                        <input type="text" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" name="email" placeholder="Enter Email Address...">
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password" name="password">
                      </div>
                      <div class="form-group">
                        <div class="custom-control custom-checkbox small">
                          <input type="checkbox" class="custom-control-input" id="customCheck"  >
                          <label class="custom-control-label" for="customCheck">Remember Me</label>
                        </div>
                      </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <a class="close" data-target="alert" data-dismiss="alert">&times;</a>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                      <button type="submit" class="btn btn-primary btn-user btn-block">
                        Login
                      </button>
                    </form>
                    <hr>
                    <div class="text-center">
                      <a class="small" href="#">Forgot Password?</a>
                    </div>
                    <div class="text-center">
                      <a class="small" href="#">Create an Account!</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
  
        </div>
  
      </div>
@endsection

@section('js_content')
    <script type="text/javascript">
    $(function(){
        //alert("++++++")
    })(jQuery);
    </script>
@endsection
