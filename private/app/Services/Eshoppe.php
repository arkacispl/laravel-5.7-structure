<?php
namespace App\Services;

class Eshoppe{

    public $path_to_service = '';

    public function __construct() {
        $this->path_to_service = "App\Services\\";
    }

    public function productService(){
        //return resolve($this->path_to_service."Product\ProductService");
        return resolve(__NAMESPACE__."\Product\ProductService");
    }

    public function emailService(){
        
    }
}
