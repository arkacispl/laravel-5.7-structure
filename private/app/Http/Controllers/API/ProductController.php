<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseApiController;
use Eshoppe;

class ProductController extends BaseApiController
{
    public function __construct(Eshoppe $eshoppeObj){
        $this->service = $eshoppeObj->productService();
    }

    public function create(Request $request){
        $this->service->createTest();
    }
}
